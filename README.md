# HumiditySensor

ESP32 code to read the temperature and Humidity value DHT11

The module is 3.3v compatible

The pinout for the module (when facing towards you) is Sense (data), VCC, GND 

The data pin requires a 10K ohm between vcc and sense (data).

The sense pin is connected to pin 33 (on the ESP32) 
