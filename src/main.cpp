#include <Arduino.h>
#include <Wire.h>  
#include "SSD1306Wire.h" 
#include <OneWire.h>
#include "AWSParameters.h"
#include "IOTGateway.h"
#include "StandardConfig.h"
#include "Sensor.h"
#include <NTPClient.h>
#include <WiFiUdp.h>


#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN 33 
#define DHTTYPE    DHT11
DHT_Unified dht(DHTPIN, DHTTYPE);

uint32_t delayMS;

float temperature;
float humidity;

// Initialize the OLED display using Wire library
SSD1306Wire  display(0x3c, 5, 4);
OneWire  ds(32);

IOTGateway iotGateway(&capgeminiHackathon);
Sensor sensor("humidity", "humidity", 5000, 5000);

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 0, 60000);

// Variables to save date and time
String formattedDate;
String dayStamp;
String timeStamp;

void setup() {
  Serial.begin(115200);
  dht.begin();
  // Initialising the UI will init the display too.
  display.init();
  display.flipScreenVertically();
  display.clear();
  Serial.println("Setup complete");

  sensor_t sensor;
  dht.humidity().getSensor(&sensor);
  delayMS = sensor.min_delay / 1000;

  iotGateway.initialise();
  delay(2500);
  timeClient.begin();
  timeClient.update();
}

void displayTemperatureAndHumidity(float celsius, float humidity) {

//clear the oleddisplay
  display.clear();
    // Font Demo1
    // create more fonts at http://oleddisplay.squix.ch/
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    // display.setFont(ArialMT_Plain_10);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "Temperature");
    display.setFont(ArialMT_Plain_16);
    // display.drawString(30, 20, "26c");

    char str_temp[6];

/* 4 is mininum width, 2 is precision; float value is copied onto str_temp*/
dtostrf(celsius, 4, 2, str_temp);
char tempBuffer[7];
sprintf(tempBuffer,"%sc", str_temp);
display.drawString(65, 0, tempBuffer);

    // display.setFont(ArialMT_Plain_16);
    // display.drawString(0, 10, "Hello world");
    // display.setFont(ArialMT_Plain_24);
    // display.drawString(0, 26, "Hello world");
display.setFont(ArialMT_Plain_10);
    display.drawString(0, 30, "Humidity");
    display.setFont(ArialMT_Plain_16);
    //
    dtostrf(humidity, 4, 2, str_temp);
    sprintf(tempBuffer,"%s%%", str_temp);
    display.drawString(50, 30, tempBuffer);


    display.display();
}

char msg[512];
char scrn_msg[5];
char ts_txt[16];
long lastTime = 0;

void publishHumidity() {
    char fullTimestamp [25];
    sprintf (fullTimestamp, "%ld", timeClient.getEpochTime());
    lastTime = millis();
    sensor.formatData(msg, 512, humidity, "humidity", fullTimestamp);
    iotGateway.publish("measurement/humidity", msg);
}

void loop() {
   // Delay between measurements.
  delay(delayMS);

  // Get temperature event and print its value.
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println(F("Error reading temperature!"));
  }
  else {
    temperature = event.temperature;
  }

  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println(F("Error reading humidity!"));
  }
  else {
    Serial.print(F("Humidity: "));
    Serial.print(event.relative_humidity);
    humidity = event.relative_humidity;
    Serial.println(F("%"));

    if (millis() - lastTime > 5000) {
      Serial.println();
      publishHumidity();
      Serial.println();
      Serial.print("  Humidity = ");
      Serial.print(humidity);
      Serial.println("%");
    }
  }

  displayTemperatureAndHumidity(temperature, humidity);
}